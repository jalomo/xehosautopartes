<?php
class M_home extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  public function getProductos()
  {
    return $this->db->select('p.*,i.ruta_archivo as imagen, i.nombre_archivo')
      ->join('imagen_producto i', 'p.id = i.producto_id and i.principal = 1')
      ->get('producto p')
      ->result();
  }
}
