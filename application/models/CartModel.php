<?php
class CartModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->database_autolavado = DATABASE_AUTOLAVADO;
    }
    public function addCart()
    {
        //Validar si ya existe ese producto
        $item_carrito = $this->validarCarrito();
        if (empty($item_carrito)) {
            //No existe
            $cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : 1;
            $data_producto = [
                'id_usuario' => 1,
                'id_producto' => $_POST['idproducto'],
                'created_at' => date('Y-m-d H:i:s'),
                'cantidad' => $cantidad
            ];
            $this->db->insert('carrito', $data_producto);
        } else {
            //Actualizar cantidad
            $cantidad = 1;
            if (isset($_POST['cantidad']) && empty($_POST['update_cart'])) {
                $cantidad = $item_carrito->cantidad + $_POST['cantidad'];
            } else if (isset($_POST['cantidad']) && isset($_POST['update_cart'])) {
                $cantidad = $_POST['cantidad'];
            }

            $this->db
                ->where('ID', $item_carrito->ID)
                ->where('vendido', 0)
                ->set('cantidad',  $cantidad)
                ->update('carrito');
        }
        return 1;
    }

    public function validarCarrito()
    {
        $q = $this->db
            ->limit(1)
            ->where('id_usuario', 1)
            ->where('vendido', 0)
            ->where('id_producto', $_POST['idproducto'])
            ->get('carrito');

        return $q->row();
    }

    public function limpiarcarrito()
    {
        return $this->db
            ->where('id_usuario', 1)
            ->where('vendido', 0)
            ->set('vendido',  1)
            ->update('carrito');
            
    }


    //Ver listado del carrito
    public function listaCarrito()
    {
        return $this->db->where('id_usuario', 1)
            ->join('producto p', 'c.id_producto = p.id')
            ->join('imagen_producto i', 'p.id = i.producto_id and i.principal = 1')
            ->select('p.id,p.valor_unitario,p.descripcion,p.precio_factura as precio,c.ID as carrito_id, c.cantidad,i.ruta_archivo as imagen, i.nombre_archivo')
            ->where('c.vendido', 0)
            ->get('carrito c')
            ->result();
    }
    //Datos del usuario
    public function getInfoUser($user_id)
    {
        return $this->db->where('u.id', $user_id)
            ->join($this->database_autolavado . '.ubicacion_usuario ub', 'u.id = ub.id_usuario')
            ->join($this->database_autolavado . '.estados e', 'ub.id_estado = e.id')
            ->join($this->database_autolavado . '.cat_municipios m', 'ub.id_municipio = m.id')
            ->select('u.*,ub.*,e.estado,m.municipio')
            ->get($this->database_autolavado . '.usuarios u')
            ->row();
    }
}
