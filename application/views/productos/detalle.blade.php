@layout('public/layout')
@section('contenido')
     <!-- Hero Area Start-->
     <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                        <h2>
                            {{ $data['producto']->descripcion}}
                        </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
     <!--================Single Product Area =================-->
     <div class="product_image_area">
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
            <div class="product_img_slide owl-carousel">
                @if (!empty($data['producto']->imagenes))
                    @foreach ($data['producto']->imagenes as $imagen)  
                        <div class="single_product_img">
                            <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo}}"  alt="#" class="img-fluid"">
                        </div>
                    @endforeach
                @else
                    <div class="single_product_img">
                        <img src="https://via.placeholder.com/1140x830?text=sin+imagen" alt="">
                    </div>
                @endif
            </div>
            </div>
            <div class="col-lg-8">
            <div  class="single_product_text text-center">
                <h3>{{ $data['producto']->descripcion}}</h3>
                <p>
                    Seamlessly empower fully researched growth strategies and interoperable internal or “organic” sources. Credibly innovate granular internal or “organic” sources whereas high standards in web-readiness. Credibly innovate granular internal or organic sources whereas high standards in web-readiness. Energistically scale future-proof core competencies vis-a-vis impactful experiences. Dramatically synthesize integrated schemas. with optimal networks.
                </p>
                <div class="card_area">
                    <div class="product_count_area">
                        <p>Cantidad</p>
                        <div class="product_count d-inline-block">
                            <span onclick="decrement({{ $data['producto']->id }})" class="product_count_item inumber-decrement"> <i class="ti-minus"></i></span>
                            <input id="input_producto_{{ $data['producto']->id }}_cantidad" class="product_count_item input-number" type="text" value="1" min="0" max="10">
                            <span onclick="increment({{ $data['producto']->id }})" class="product_count_item number-increment"> <i class="ti-plus"></i></span>
                        </div>
                        <p>${{ $data['producto']->valor_unitario }}</p>
                    </div>
                    @if (is_null($producto->cantidad_actual) || $producto->cantidad_actual == 0)
                        <div class="add_to_cart">
                            <h3 >Agotado</h3>
                        </div>
                        @else
                        <div class="add_to_cart">
                            <button onclick="agregarcarrito({{ $data['producto']->id  }})" class="btn_3">Agregar al carrito</button>
                        </div>
                    @endif
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->
@endsection

@section('scripts')
    <script src="{{ base_url() }}assets/js/cart.js"></script>
@endsection