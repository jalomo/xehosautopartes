@layout('public/layout')
@section('contenido')
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Auto partes</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!-- Latest Products Start -->
    <section class="popular-items latest-padding">
        <div class="container">
            <!-- Nav Card -->
            <div class="tab-content" id="nav-tabContent">
                <!-- card one -->
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        @foreach($productos as $p => $producto)
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                            <div class="single-popular-items mb-50 text-center">
                                <div class="popular-img">
                                    <div id="carousel_{{$p}}" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            @if (!empty($producto->imagenes))        
                                                @foreach ($producto->imagenes as $imagen)  
                                                    <div class="carousel-item  {{ $imagen->principal == 1 ? "active": "" }}">
                                                        <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo}}" alt="">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="carousel-item active"">
                                                    <img src="https://via.placeholder.com/468x400?text=sin+imagen" alt="">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="img-cap">
                                        @if (is_null($producto->cantidad_actual) || $producto->cantidad_actual == 0)
                                                <span style="background: #f81f1fc9" class="js_add_cart">Agotado</span>
                                            @else
                                                <span onclick="agregarcarrito({{ $producto->id  }})" class="js_add_cart">Agregar al carrito</span>
                                        @endif
                                    </div>
                                    <div class="favorit-items">
                                        <span class="flaticon-heart"></span>
                                    </div>
                                </div>
                                <div class="popular-caption">
                                    <h3><a href="product_details.html">{{$producto->descripcion}}</a></h3>
                                    <span>$ {{number_format($producto->precio_factura,2)}}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- End Nav Card -->
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ base_url('assets/js/cart.js') }}"></script>
@endsection
