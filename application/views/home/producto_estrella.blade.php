<?php 
// dd($data['producto_estrella_1']);
?>

<!--? Watch Choice  Start-->
 <div class="watch-area section-padding30">
    <div class="container">
        <div class="row align-items-center justify-content-between padding-130">
            @if (!empty($data['producto_estrella_1']))
                <div class="col-lg-5 col-md-6">
                    <div class="watch-details mb-40">
                    <h2>{{ $data['producto_estrella_1']->descripcion }}</h2>
                        <p>Aqui detalles de producto.</p>
                        <a href="{{ base_url('producto/'.$data['producto_estrella_1']->id) }}" class="btn">Ver detalle</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-10">
                    <div class="choice-watch-img mb-40">
                        <div id="carousel_2" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @if (!empty($data['producto_estrella_2']->imagenes))
                                    @foreach ($data['producto_estrella_2']->imagenes as $imagen)  
                                        <div class="carousel-item  {{ $imagen->principal == 1 ? "active": "" }}">
                                            <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo}}" alt="">
                                        </div>
                                    @endforeach
                                @else
                                    <div class="carousel-item active"">
                                        <img src="https://via.placeholder.com/468x400?text=sin+imagen" alt="">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="row align-items-center justify-content-between">
            @if (!empty($data['producto_estrella_2']))
                <div class="col-lg-6 col-md-6 col-sm-10">
                    <div class="choice-watch-img mb-40">
                        <div id="carousel_1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @if (!empty($data['producto_estrella_1']->imagenes))
                                    @foreach ($data['producto_estrella_1']->imagenes as $imagen)  
                                        @if ($imagen->categoria_id == 4)
                                            <div class="carousel-item  {{ $imagen->principal == 1 ? "active": "" }}">
                                                <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo}}" alt="">
                                            </div>
                                        @endif
                                    @endforeach
                                @else
                                    <div class="carousel-item active"">
                                        <img src="https://via.placeholder.com/468x400?text=sin+imagen" alt="">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="watch-details mb-40">
                        <h2>{{ $data['producto_estrella_2']->descripcion }}</h2>
                        <p>Aqui detalles de producto.</p>
                        <a href="{{ base_url('producto/'.$data['producto_estrella_2']->id) }}" class="btn">Ver detalle</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<!-- Watch Choice  End-->