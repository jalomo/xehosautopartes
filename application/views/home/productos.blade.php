<!--? Popular Items Start -->
<div class="popular-items section-padding30">
    <div class="container">
        <!-- Section tittle -->
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-10">
                <div class="section-tittle mb-70 text-center">
                    <h2>Productos</h2>
                    <p>Listado de productos en stock</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($productos as $p => $producto)
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-popular-items mb-50 text-center">
                        <div class="popular-img productos-container">
                            <div id="carousel_{{$p}}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @if (!empty($producto->imagenes))        
                                        @foreach ($producto->imagenes as $imagen)  
                                            @if ($imagen->categoria_id == 3)
                                                <div class="carousel-item  {{ $imagen->principal == 1 ? "active": "" }}">
                                                    <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo}}" alt="">
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <div class="carousel-item active"">
                                            <img src="https://via.placeholder.com/468x400?text=sin+imagen" alt="">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="img-cap">
                                
                                @if (is_null($producto->cantidad_actual) || $producto->cantidad_actual == 0)
                                        <span style="background: #f81f1fc9" class="js_add_cart">Agotado</span>
                                    @else
                                    <span onclick="agregarcarrito({{ $producto->id  }})"  class="js_add_cart">Agregar al carrito</span>
                                @endif
                            </div>
                            <div class="favorit-items">
                                <span class="flaticon-heart"></span>
                            </div>
                        </div>
                        <div class="popular-caption">
                        <h3><a href="{{ base_url('producto/'.$producto->id) }}">{{$producto->descripcion}}</a></h3>
                            <span>$ {{number_format($producto->precio_factura,2)}}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Button -->
        <div class="row justify-content-center">
            <div class="room-btn pt-70">
                <a href="{{ base_url('productos') }}" class="btn view-btn1">Ver productos</a>
            </div>
        </div>
    </div>
</div>
<!-- Popular Items End -->

@section('scripts')
    <script src="{{ base_url('assets/js/cart.js') }}"></script>
@endsection