<!--? Video Area Start -->
<div class="video-area">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-12">
            <div class="video-wrap">
                <div class="play-btn "><a class="popup-video" href="https://www.youtube.com/watch?v=H-nx4w346Cw"><i class="fas fa-play"></i></a></div>
            </div>
            </div>
        </div>
        <!-- Arrow -->
        <div class="thumb-content-box">
            <div class="thumb-content">
                <h3>Ir a video</h3>
                <a href="https://www.youtube.com/watch?v=H-nx4w346Cw" target="_blank"> <i class="flaticon-arrow"></i></a>
            </div>
        </div>
    </div>
</div>
<!-- Video Area End -->