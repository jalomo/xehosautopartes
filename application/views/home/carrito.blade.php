@layout('public/layout')
@section('contenido')
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Carrito</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================Cart Area =================-->
    <section class="cart_area section_padding">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Producto</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Total</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $subtotal = 0; ?>
                            @foreach ($carrito as $c => $cart)
                                <?php $total_producto = 0; ?>
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="d-flex">
                                                <img src="{{ API_IMAGE_PATH .$cart->nombre_archivo }}" alt="" />
                                            </div>
                                            <div class="media-body">
                                                <p>{{ $cart->descripcion }}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h5>${{ number_format($cart->precio, 2) }}</h5>
                                    </td>
                                    <td>
                                        
                                        <div class="product_count">
                                            <span class="input-number-decrement" onclick="decrement({{ $cart->id }})"> <i class="ti-minus"></i></span>
                                            <input id="input_producto_{{ $cart->id }}_cantidad"  class="input-number" type="text" value="{{ (int)$cart->cantidad }}" min="0" max="10">
                                            <span  onclick="increment({{ $cart->id }})" class="input-number-increment"> <i class="ti-plus"></i></span>
                                        </div>
                                    </td>
                                    <td>
                                        <?php
                                        $total_producto = $cart->precio * $cart->cantidad;
                                        $subtotal += $total_producto;
                                        ?>
                                        <h5>${{ number_format($total_producto, 2) }}</h5>
                                    </td>
                                    <td>
                                        <button onclick="borrarelementocarrito({{ $cart->carrito_id }})" class="btn"> Eliminar </button>
                                        <button onclick="actualizacarrito({{ $cart->id }})"  class="btn_1">Actualizar </button>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3"></td>
                                <td>
                                    <h5><strong>Total</strong></h5>
                                </td>
                                <td>
                                    <h5>${{ number_format($subtotal, 2) }}</h5>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="checkout_btn_inner float-right">
                        <a class="btn_1" href="{{ base_url('productos') }}">Continuar comprando</a>
                        @if (!empty($data['total_carrito']) && $data['total_carrito']['total'] != 0)
                            <a class="btn_1 checkout_btn_1" href="{{ base_url('checkout') }}">Proceder al pago</a>
                        @endif
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ base_url() }}assets/js/cart.js"></script>
@endsection
