<!-- ? New Product Start -->
<section class="new-product-area section-padding30">
    <div class="container">
        <!-- Section tittle -->
        <div class="row">
            <div class="col-xl-12">
                <div class="section-tittle mb-70">
                    <h2>Lo mas vendido</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($productos as $p => $producto)
                @if ($p == 3)
                    <?php break ?>
                @endif
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-new-pro mb-30 text-center">
                        <div class="product-img masvendido-imagenes">
                            @if (!empty($producto->imagenes))
                                @foreach ($producto->imagenes as $imagen)
                                    @if ($imagen->categoria_id == 1)
                                        <img src="{{ API_IMAGE_PATH . $imagen->nombre_archivo }}" alt=""> 
                                        <?php break ?>
                                    @endif
                                @endforeach
                                @else
                                <img src="https://via.placeholder.com/360x488?text=sin+imagen" alt="">
                            @endif
                        </div>
                        <div class="product-caption">
                            <h3><a href="{{ base_url('producto/'.$producto->id) }}">{{ $producto->descripcion }}</a></h3>
                            <span>$ {{ $producto->valor_unitario }}</span>
                        </div>
                    </div>
                </div>    
            @endforeach
        </div>
    </div>
</section>
<!--  New Product End -->