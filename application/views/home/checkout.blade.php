@layout('public/layout')
@section('contenido')
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Resumen de venta</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================Checkout Area =================-->
    <section class="checkout_area section_padding">
        <div class="container">
            <div class="cupon_area">
                <div class="check_title">
                    <h2>
                        ¿Tienes un cupón?
                    </h2>
                </div>
                <input type="text" placeholder="Ingresa un cupón válido" />
                <a class="tp_btn" href="#">Aplicar cupón</a>
            </div>
            <div class="billing_details">
                <div class="row">
                    <div class="col-lg-8">
                        <h3>Información usuario</h3>
                        <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                            <div class="col-md-12 form-group p_star">
                                <label for="">Nombre</label>
                                <input disabled type="text" class="form-control" id="nombre" name="nombre" value="{{$usuario->nombre.' '.$usuario->apellido_paterno.' '.$usuario->apellido_materno}}" />  
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <label for="">Teléfono</label>
                                <input disabled type="text" class="form-control" id="telefono" name="telefono" value="{{$usuario->telefono}}" />  
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <label for="">Correo electrónico</label>
                                <input disabled type="text" class="form-control" id="email" name="email" value="{{$usuario->email}}" />  
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <label for="">Calle</label>
                                <input disabled type="text" class="form-control" id="calle" name="calle" value="{{$usuario->calle}}" />  
                            </div>
                            <div class="col-md-3 form-group p_star">
                                <label for=""># interior</label>
                                <input disabled type="text" class="form-control" id="numero_int" name="numero_int" value="{{$usuario->numero_int}}" />  
                            </div>
                            <div class="col-md-3 form-group p_star">
                                <label for=""># exterior</label>
                                <input disabled type="text" class="form-control" id="numero_ext" name="numero_ext" value="{{$usuario->numero_ext}}" />  
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <label for="">Estado</label>
                                <input disabled type="text" class="form-control" id="estado" name="estado" value="{{$usuario->estado}}" />  
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <label for="">Municipio</label>
                                <input disabled type="text" class="form-control" id="municipio" name="municipio" value="{{$usuario->municipio}}" />  
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <textarea class="form-control" name="message" id="message" rows="1"
                                    placeholder="Agregar indicaciones"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <div class="order_box">
                            <h2>Tu pedido</h2>
                            <ul class="list">
                                <li>
                                    <a href="#">Producto
                                        <span>Total</span>
                                    </a>
                                </li>
                                @if (!empty($carrito['productos']))    
                                    @foreach ($carrito['productos'] as $item)
                                        <li >
                                        <a href="">
                                            {{ isset($item['producto']) ? $item['producto'] : '-' }}
                                            <span>x{{ isset($item['cantidad']) ? $item['cantidad'] : 0 }}  ${{ isset($item['total_producto']) ? number_format($item['total_producto'], 2) : 0 }}</span>
                                        </a>
                                        </li>
                                    @endforeach
                                    @else
                                    <span>Sin elementos</span>
                                @endif
                            </ul>
                            
                            <ul class="list list_2">
                                <li>
                                    <a href="#">Subtotal
                                        <span>${{ number_format($carrito['subtotal'], 2)  }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">Descuento
                                        <span>00.00</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Total
                                        <span id="totalpagar" data-total="{{ $carrito['total'] }}">
                                            ${{ number_format($carrito['total'], 2) }}
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="payment_item active">
                                <div class="radion_btn">
                                    <input type="radio" id="f-option6" name="selector" checked />
                                    <label for="f-option6">Paypal </label>
                                    <div class="check"></div>
                                </div>
                                <p>
                                    
                                </p>
                            </div>
                            <div class="creat_account">
                                <input type="checkbox" id="f-option4" name="selector" />
                                <label for="f-option4">Acepto </label>
                                <a href="#">los términos y condiciones*</a>
                            </div>
                            <div id="smart-button-container">
                                <div style="text-align: center;">
                                  <div id="paypal-button-container"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Checkout Area =================-->
@endsection
@section('scripts')
<script src="{{ base_url('assets/js/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="https://www.paypal.com/sdk/js?client-id=AW7wOfFjU5slELn4P9AsFmdjwFhB5GjlBzgQHlkNxx1n0A5hfDU2kdJ4OvIyQN1ppEZgpY5PVG_789of&currency=MXN" data-sdk-integration-source="button-factory"></script>
<script src="{{ base_url('assets/js/checkout/paypal.js') }}"></script>
<script>
    $("#f-option4").on('click',function(){})
</script>
@endsection