<footer>
    <!-- Footer Start-->
    <div class="footer-area footer-padding">
        <div class="container">
            <!-- Footer bottom -->
            <div class="row align-items-center">
                <div class="col-xl-7 col-lg-8 col-md-7">
                    <div class="footer-copy-right">
                        <p>
                            Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script> Derechos reservados | 
                            <a href="https://xehos.com/" target="_blank">
                                Xehos
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-4 col-md-5">
                    <div class="footer-copy-right f-right">
                        <!-- social -->
                        <div class="footer-social">
                            <a target="_blank" href="https://twitter.com/xehosautolavado">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a target="_blank" href="https://www.facebook.com/XehosAutolavado/">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a  target="_blank"href="https://www.instagram.com/xehosautolavado/">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a target="_blank" href="https://xehos.com/" ><i class="fas fa-globe"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>