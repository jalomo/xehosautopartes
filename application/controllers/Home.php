<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('M_home', 'mh', TRUE);
        $this->load->helper(array('form', 'html', 'url'));
    }
    public function index()
    {
        $this->load->library('curl');
        $productosStock = $this->curl->curlGet('api/productos/stockActual');
        // dd($productosStock);
        $data['productos'] = procesarResponseApiJsonToArray($productosStock);
        $producto_estrella_1 = $this->curl->curlGet('api/productos/1');
        $data['producto_estrella_1'] = procesarResponseApiJsonToArray($producto_estrella_1);
        $producto_estrella_2 = $this->curl->curlGet('api/productos/2');
        $data['producto_estrella_2'] = procesarResponseApiJsonToArray($producto_estrella_2);
        $this->blade->render('home/index', $data, FALSE);
    }

    public function detalle($id = '')
    {
        $this->load->library('curl');
        $productosStock = $this->curl->curlGet('api/productos/stockActual?producto_id=' . $id);

        $validar_producto = !empty(procesarResponseApiJsonToArray($productosStock)) ? procesarResponseApiJsonToArray($productosStock)[0] : [];
        $data['producto'] = $validar_producto;
        $this->blade->render('productos/detalle', $data, FALSE);
    }

    public function productos()
    {
        $this->load->library('curl');
        $productosStock = $this->curl->curlGet('api/productos/stockActual');
        $data['productos'] = procesarResponseApiJsonToArray($productosStock);

        $this->session->set_userdata('menu', 'shop');
        $this->blade->render('productos/listado', $data, FALSE);
    }
}
