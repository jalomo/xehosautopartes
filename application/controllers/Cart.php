<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cart extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(['session', 'curl']);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->model('CartModel', 'cm', TRUE);
        $this->load->helper(array('form', 'html', 'url'));
    }
    public function index()
    {
        $data['carrito'] = $this->cm->listaCarrito();
        $data['total_carrito'] = $this->getTotalCarrito();
        $this->session->set_userdata('menu', 'carrito');
        $this->blade->render('home/carrito', $data, FALSE);
    }
    public function addCart()
    {
        echo $this->cm->addCart();
    }
    public function deleteProduct()
    {
        $this->db->where('ID', $_POST['id'])
            ->delete('carrito');
        echo 1;
        exit();
    }

    public function limpiarcarrito()
    {
        $descontar = $this->curl->curlGet('api/ventas/autoparte');
        // dd($descontar);
        $validar_venta = !empty(procesarResponseApiJsonToArray($descontar)) ? procesarResponseApiJsonToArray($descontar) : [];
        // dd($validar_venta);
        if (!empty($validar_venta)) {
            echo $this->cm->limpiarcarrito();
        }
    }

    public function checkout()
    {
        $data['usuario'] = $this->cm->getInfoUser(16);
        $data['carrito'] = $this->getTotalCarrito();

        $this->session->set_userdata('menu', 'checkout');
        $this->blade->render('home/checkout', $data, FALSE);
    }

    public function productos_carrito()
    {
        $carrito = $this->cm->listaCarrito();
        echo json_encode($carrito);
    }

    public function getTotalCarrito()
    {
        $carrito = $this->cm->listaCarrito();
        $subtotal = 0;
        $data = [];
        foreach ($carrito as $key => $item) {
            $total_producto = 0;
            $total_producto = $item->precio * $item->cantidad;
            $subtotal += $total_producto;
            $data['productos'][$key]['producto'] = $item->descripcion;
            $data['productos'][$key]['cantidad'] = (int) $item->cantidad;
            $data['productos'][$key]['total_producto'] = $total_producto; //number_format($total_producto, 2);
        }

        $data['subtotal'] = $subtotal;
        $data['descuento'] = 0;
        $data['total'] = $subtotal; //number_format($subtotal, 2);
        return $data;
    }
}
