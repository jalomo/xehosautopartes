<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function procesarResponseApiJsonToArray($response_data)
{
    return json_decode($response_data);
}