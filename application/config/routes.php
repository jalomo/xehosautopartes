<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'Home';
$route['blogs/(:num)'] = 'home/blog/$1';
$route['leer-blog/(:num)'] = 'home/detalle_blog/$1';
$route['productos'] = 'home/productos';
$route['checkout'] = 'cart/checkout';
$route['carrito'] = 'cart';
$route['404_override'] = '';
$route['producto/(:num)'] = 'home/detalle/$1';
$route['translate_uri_dashes'] = FALSE;