
const total_pagar = $("#totalpagar").data('total');
// console.log(total_pagar, total_pagar.toFixed(2))
const procesarventacarrito = () => {
    var loading_item = document.createElement("i");
    loading_item.classList.add("fas", "fa-spinner", "fa-spinner", "fa-spin", "spinner-lg", "fa-5x");
    swal({
        content: loading_item,
        buttons: false,
        closeOnClickOutside: false
    });

    const url = site_url + "/cart/limpiarcarrito";
    myfunction.ajaxpost(url, {}, (response) => {
        console.log(response,"limpia carrito")
        swal.close()
        if (response) {
            ExitoCustom("Venta finalizada", () => window.location.href = site_url);
        } else {
            ErrorCustom("La venta no puede completarce");
        }
    })
}
function initPayPalButton() {
    paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'gold',
            layout: 'vertical',
            label: 'paypal',

        },

        createOrder: function (data, actions) {
            return actions.order.create({
                purchase_units: [{ "amount": { "currency_code": "MXN", "value": total_pagar.toFixed(2) } }]
            });
        },

        onApprove: function (data, actions) {
            return actions.order.capture().then(function (details) {
                swal({
                    title: "Éxito!",
                    text: 'Completando transacción ' + details.payer.name.given_name,
                    closeOnClickOutside: false,
                    icon: "info",
                    button: "Continuar",
                    closeOnClickOutside: false
                }).then(value => {
                    procesarventacarrito();
                })
            });
        },

        onError: function (err) {
            console.log(err);
        }
    }).render('#paypal-button-container');
}
initPayPalButton();

