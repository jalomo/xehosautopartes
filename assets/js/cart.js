
const agregarcarrito = (producto_id) => {
	const url = site_url + "/cart/addCart";
	const cantidad = `#input_producto_${producto_id}_cantidad`;
	const cantidad_value = $(`${cantidad}`).val() ? $(`${cantidad}`).val() : 1;

	myfunction.ajaxpost(url, {
		idproducto: producto_id,
		cantidad: cantidad_value
	}, (response) => {
		if (response) {
			ExitoCustom("Producto agregado al carrito", () => window.location.href = site_url);
		} else {
			ErrorCustom("El producto no se pudo agregar, por favor intenta otra vez");
		}
	})
}
// limpiarcarrito

const actualizacarrito = (producto_id) => {
	const url = site_url + "/cart/addCart";
	const cantidad = `#input_producto_${producto_id}_cantidad`;
	const cantidad_value = $(`${cantidad}`).val() ? $(`${cantidad}`).val() : 1;

	myfunction.ajaxpost(url, {
		idproducto: producto_id,
		cantidad: cantidad_value,
		update_cart: true
	}, (response) => {
		if (response) {
			ExitoCustom("Producto actualizado", () => window.location.reload());
		} else {
			ErrorCustom("El producto no se pudo actualizar, por favor intenta otra vez");
		}
	})
}

const borrarelementocarrito = (item_id) => {
	var url = site_url + "/cart/deleteProduct";
	myfunction.ajaxpost(url, {
		id: item_id
	}, (response) => {
		response && ExitoCustom("Producto eliminado", () => window.location.href = `${base_url}carrito`);
	})
}