const producto_maximo = 10
const producto_minimo = 0;
function decrement(producto_id) {
    const item_cantidad = `#input_producto_${producto_id}_cantidad`;
    var value = $(`${item_cantidad}`).val();

    value--;
    if (!producto_minimo || value >= producto_minimo) {
        $(`${item_cantidad}`).val(value);
    }
}

function increment(producto_id) {
    const item_cantidad = `#input_producto_${producto_id}_cantidad`;
    var value = $(`${item_cantidad}`).val();
    
    value++;
    if (!producto_maximo || value <= producto_maximo) {
        $(`${item_cantidad}`).val(value);
    }
}

